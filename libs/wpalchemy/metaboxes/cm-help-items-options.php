<div class="my_meta_control cm-help-items-options">

    <?php
    wp_print_styles('editor-buttons');

    ob_start();
    wp_editor('', 'content', array(
        'dfw' => true,
        'editor_height' => 1,
        'tinymce' => array(
            'resize' => true,
            'add_unload_trigger' => false,
        ),
    ));
    $content = ob_get_contents();
    ob_end_clean();

    $args = array(
        'post_type' => 'page',
        'show_option_none' => CMPopUpFlyIn::__('None'),
        'option_none_value' => '',
    );

    // 'html' is used for the "Text" editor tab.
//    if( 'html' === wp_default_editor() )
//    {
//    add_filter('the_editor_content', 'wp_htmledit_pre');
//    $switch_class = 'html-active';
//    }
//    else
//    {
    add_filter('the_editor_content', 'wp_richedit_pre');
    $switch_class = 'tmce-active';
//    }
    
    $defaultWidgetType = CMPOPFLY_Settings::getOption(CMPOPFLY_Settings::OPTION_DEFAULT_WIDGET_TYPE);
    $widgetType = CMPOPFLY_Settings::getOptionConfig(CMPOPFLY_Settings::OPTION_DEFAULT_WIDGET_TYPE);
    $widgetDisplayMethod = CMPOPFLY_Settings::getOptionConfig(CMPOPFLY_Settings::OPTION_DISPLAY_METHOD);
    $widgetShape = CMPOPFLY_Settings::getOptionConfig(CMPOPFLY_Settings::OPTION_CUSTOM_WIDGET_SHAPE);
    $underlayType = CMPOPFLY_Settings::getOptionConfig(CMPOPFLY_Settings::OPTION_CUSTOM_WIDGET_UNDERLAY_TYPE);
    $selectedBanner = CMPOPFLY_Settings::getOptionConfig(CMPOPFLY_Settings::OPTION_CUSTOM_WIDGET_SELECTE_BANNER);
    ?>
    <label>Active</label>
    <p>
        <?php $mb->the_field('cm-campaign-show-allpages'); ?>
        <input type="hidden" name="<?php $mb->the_name(); ?>" value="0"/>
        <input type="checkbox" name="<?php $mb->the_name(); ?>" value="1" <?php checked('1', $metabox->get_the_value()); ?> class="<?php $mb->the_name(); ?>"/>
        <span class='field-info'>If this checkbox is selected then this Campaign will be displayed on each post and page of your website. Only one Campaign can be set to active.</span>
    </p>
    <label>Widget type</label>
    <p>
        <?php $mb->the_field('cm-campaign-widget-type'); ?>
        <select name="<?php $mb->the_name(); ?>" id="cm-campaign-widget-type">
            <?php
            $fieldValue = $mb->get_the_value();
//            echo '<option value="0" ' . selected('0', $fieldValue, false) . '>' . CMPopUpFlyIn::__('Default') . ' (' . $widgetType['options'][$defaultWidgetType] . ') </option>';
            foreach ($widgetType['options'] as $key => $value) {
                echo '<option value="' . $key . '" ' . selected($key, $fieldValue, false) . '>' . $value . '</option>';
            }
            ?>
        </select><br />
        <span class='field-info'>You can choose the different Widget Type for the current Campaign.</span>
    </p>
    <span id='campaign-selected-banner-panel-container'>
        <label>Selected Banner</label>
        <p>
            <span class="floatLeft" style="display: none;">
                <?php $mb->the_field('cm-campaign-display-method');
                $fieldValue = "selected";
                foreach ($widgetDisplayMethod['options'] as $key => $value) {
                    echo '<input name="' . $mb->get_the_name() . '" type="radio" value="' . $key . '" ' . checked($key, $fieldValue, false) . ' class="campaign-display-method">' . $value . "<br />";
                }
                ?>
            </span>
            <span id='campaign-selected-banner-panel' class="floatLeft">
                <?php $mb->the_field('cm-campaign-widget-selected-banner'); ?>
                <input type="hidden" id="campaign-selected-banner-back" value="<?php echo $fieldValue = $mb->get_the_value() ?>"/>
                <select name="<?php $mb->the_name(); ?>" id="cm-campaign-widget-selected-banner"></select>
            </span>
            <div class="clear"></div>
            <br />
            <span class='field-info'>You can choose which banner will be displayed in current campaign. Only one banner can be selected.</span>
        </p>
    </span> 
    <label>Widget width</label>
    <p>
        <?php $mb->the_field('cm-campaign-widget-width'); ?>
        <input type="text" name="<?php $mb->the_name(); ?>" placeholder="250px" value="<?php echo $metabox->get_the_value(); ?>"/>
        <span class='field-info'>Campaign widget width. If blank defaults to 250px</span>
    </p>
    <label>Widget height</label>
    <p>
        <?php $mb->the_field('cm-campaign-widget-height'); ?>
        <input type="text" name="<?php $mb->the_name(); ?>" placeholder="350px" value="<?php echo $metabox->get_the_value(); ?>"/>
        <span class='field-info'>Campaign widget height. If blank defaults to 350px</span>
    </p>
    <label>Background color</label>
    <p>
        <?php $mb->the_field('cm-campaign-widget-background-color'); ?>
        <input type="text" name="<?php $mb->the_name(); ?>" placeholder="#ffffff" value="<?php echo $metabox->get_the_value(); ?>"/>
        <span class='field-info'>Campaign widget background color. Please enter it in hash color format (eg. #abc123). If blank defaults to #ffffff (white)</span>
    </p>
    <label>Widget shape</label>
    <p>
        <?php $mb->the_field('cm-campaign-widget-shape'); ?>
        <select name="<?php $mb->the_name(); ?>">
            <?php
            $fieldValue = $mb->get_the_value();
            if(empty($fieldValue)){
                $fieldValue = $widgetShape['default'];
            }
//            echo '<option value="0" ' . selected('0', $fieldValue, false) . '>' . CMPopUpFlyIn::__('Default') . ' (' . $widgetType['options'][$defaultWidgetType] . ') </option>';
            foreach ($widgetShape['options'] as $key => $value) {
                echo '<option value="' . $key . '" ' . selected($key, $fieldValue, false) . '>' . $value . '</option>';
            }
            ?>
        </select>
        <br />
        <span class='field-info'>You can choose the different Widget Shape for the current Campaign.</span>
    </p>
    <span id="underlayTypeContainer" style="display: none;">
    <label>Underlay type</label>
    <p>
        <?php $mb->the_field('cm-campaign-widget-underlay-type'); ?>
        <select name="<?php $mb->the_name(); ?>">
            <?php
            $fieldValue = $mb->get_the_value();
            if(empty($fieldValue)){
                $fieldValue = $underlayType['default'];
            }
            foreach ($underlayType['options'] as $key => $value) {
                echo '<option value="' . $key . '" ' . selected($key, $fieldValue, false) . '>' . $value . '</option>';
            }
            ?>
        </select>
        <br />
        <span class='field-info'>You can choose the different Widget Underlay Type for current Campaign.</span>
    </p>
    </span>
    <p class="meta-save"><button type="submit" class="button-primary" name="save"><?php _e('Update'); ?></button></p>

</div>