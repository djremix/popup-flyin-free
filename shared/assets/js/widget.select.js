(function ($) {

    var init_widget = function () {
        $('#cmpopfly-widget-container select').selectric(
                {
                    maxHeight: 150,
                    onChange: function (element) {
                        var $this = $(this);
                        var selected = $this.find(':selected');
                        var url = selected.data('url');

                        if (typeof url !== 'undefined' && url.length)
                        {
                            window.open(url, '_blank');
                            return false;
                        }
                        else
                        {
                            $(element).change();
                        }
                    },
                    optionsItemBuilder: function (itemData, element, index)
                    {
                        var html;
                        var url = element.data('url');
                        var image = element.data('image');

                        if ((typeof image === 'undefined'))
                        {
                            image = '';
                        }
                        else
                        {
                            image = '<img src="' + image + '" alt="External Link Icon" class="cmpopfly-external-link-icon-left" />';
                        }
                        html = (typeof url !== 'undefined' && url.length) ? '<a href="' + url + '">' + image + itemData.text + '</a>' : itemData.text;
                        return html;
                    }
                }
        );

        $('#cmpopfly-search').fastLiveFilter('.selectricWrapper .selectricScroll ul', {
            timeout: 200
        });
    };

    $("select").change(function () {
        var id = $(this).children(":selected").attr("id");
        var idStr = id.toString();
        $('.cmpopfly-widget-content ul').removeClass('show');
        $('.' + idStr).addClass('show');
    });

    var lib_url = cmpopfly_data.js_path + 'jquery.selectric.js';
    $.getScript(lib_url, init_widget);

})(jQuery);