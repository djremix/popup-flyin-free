(function ($) {

        var slider;

        var init_widget = function () {

            var of_label = cmpopfly_data.of_label || ' of ';

            slider = $('#cmpopfly-widget-container .bxslider').bxSlider({
                slideSelector: 'li:not(.cmpopfly-hidden)',
//                adaptiveHeight: true,
                pager: false,
                onSlideBefore: function () {
                    $('.count').text((slider.getCurrentSlide() + 1) + of_label + slider.getSlideCount());
                }
            });

            $('#cmpopfly-search').on('change', function(){
                slider.reloadSlider();
                $('.count').text((slider.getCurrentSlide() + 1) + of_label + slider.getSlideCount());
            });

            var stylesheet = document.createElement('link');
            stylesheet.href = cmpopfly_data.css_path + 'widget.slider/jquery.bxslider.css';
            stylesheet.rel = 'stylesheet';
            stylesheet.type = 'text/css';
            document.getElementsByTagName('head')[0].appendChild(stylesheet);
        };

        var lib_url = cmpopfly_data.js_path + 'jquery.bxslider.min.js';
        $.getScript(lib_url, init_widget);

})(jQuery);