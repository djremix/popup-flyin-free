=== CM Pop-Ups and Fly-Ins PRO Plugin ===
Author URI: http://plugins.cminds.com/
Tags: Cminds, CM, help, sidebar, widget
Stable Tag: 1.0.0

This plugin allows to create Advertisement Items Widget to be displayed on each page of the website.

== Description ==

This plugin allows to display an Advertisement banners that will popup on the center or at the bottom right corner of the page/post screen.

= What does this plugin do, exactly? =

1) Allows to create Campaigns Items
2) Allows to create Campaigns Advertisement Items
3) Each Advertisement Item can have it's own HTML content (images, texts etc.)
4) Displays the Advertisement Item in the popup on the center or at the bottom right corner of the page/post screen

= Plugin usage instructions =

1) Go to "Add New Campaign"
2) Fill the "Title" of the campaign and "content" of one or many Advertisement Items
3) Click "Add Advertisement Item" to dynamically add more items
4) Check "active"
5) Click "Save Changes"
6) Go to any page of your website
7) Watch the banner with Advertisement Item
8) Close the banner clicking "X" icon

== Installation ==

1. Upload `cm-pop-up-fly-in` folder to the `/wp-content/plugins/` directory
2. Activate the plugin through the 'Plugins' menu in WordPress.

== Changelog ==
= 1.0.0 =

* This is the first release.